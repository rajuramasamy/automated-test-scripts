var frisby = require('frisby');
var ClientAPI ="https://dev-clientapi.sedanmagic.com/";
var ProviderAPI ="https://dev-providerapi.sedanmagic.com/";
var api_key = '06913B08-5228-4CB2-BD77-E6485C529ACE';
var api_key_2 = 'A69A850F-08F3-4984-A50F-3FC374C37877';
var api_key_provider = '966B3C48-ED51-44DF-91C9-3D36E77378A8';
var auth_tom = ' Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh';
var auth_phaz = 'Basic YWxlcGh0ZXN0MDRAYWxlcGhnbG9iYWwuY29tOlRyYW5zZGV2MTU=';
var auth_suppliers = 'Basic c3VwcGxpZXJzLWludGVncmF0aW9uQGthcmhvby5jb206a2FyaG9vMTIz';
var auth_karhoo = 'Basic c3VwcGxpZXJzLWludGVncmF0aW9uQGthcmhvby5jb206a2FyaG9vMTIz';
var acceptHeader = 'application/json';
var hostHeader = 'dev-clientapi.sedanmagic.com';

//The GlobalSetup() allows common headers to be declared globally
/*frisby.globalSetup({ 
  request: {
    headers: {'Authorization' : 'Basic YWxlcGh0ZXN0MDRAYWxlcGhnbG9iYWwuY29tOlRyYW5zZGV2MTU='
    ,'host' : 'dev-clientapi.sedanmagic.com', 'Accept' : 'application/json'
    }
  }
});*/


frisby.create('GET User Profile - 406 No Accept Header')
  .get(ClientAPI + 'user/profile')
    //.addHeader('accept','application/json')
    .addHeader('api-key',api_key)
    .addHeader('host',hostHeader)
    .addHeader('Authorization',auth_tom)   
          .expectStatus(406)
            //.expectHeaderContains('content-type', 'application/json')
            .inspectBody() //Inspects the body of the response
                    .toss() //Executes the request

frisby.create('POST User Verification - 406 No Accept Header')
  .post(ClientAPI + 'user/verification', {
    username: "alephtest04@alephglobal.com"
  })
    .addHeader('api-key',api_key)
      
        .expectStatus(406)
          .inspectBody()
            .toss()



frisby.create('POST User Verification - 406 No Accept Header')
  .post(ClientAPI + 'user/verification', {
    username: ""
  })
    .addHeader('api-key',api_key)
      
        .expectStatus(406)
          .inspectBody()
            .toss()

frisby.create('POST User Registration - 406 No Accept Header')
  .post(ClientAPI + 'user/ztrip/registration', {
    username: ""
  })
    .addHeader('api-key',api_key)
      
        .expectStatus(406)
          .inspectBody()
            .toss()

frisby.create('PUT User Registration - 406 No Accept Header')
  .put(ClientAPI + 'user/ztrip/registration', {
    username: "alephtest04@alephglobal.com", password:"Transdev15", new_password : "test"
  })
    .addHeader('api-key',api_key)
        .expectStatus(406)
          .inspectBody()
            .toss() 

frisby.create('DELETE the user - 406 No Accept Header')
  .delete(ClientAPI + 'user/ztrip/registration', {
    username: ""
  })
    .addHeader('api-key',api_key)
        .expectStatus(406)
          .inspectBody()
            .toss()

frisby.create('GET Account User Booking Criterion')
  .get(ClientAPI + 'accountuserbookingcriterion/1/Sedan_Magic_Aleph/1/ta')
    .addHeader('api-key',api_key)
    .addHeader('Authorization',auth_tom)
      .expectStatus(406)
          .inspectBody()
            .toss()

frisby.create('POST User Reservation - 201 - Created')
  .post(ClientAPI + 'reservation/1/Sedan_Magic_Aleph/1/ta', {provider_id:"1",company_name:"Sedan_Magic_Aleph",provider_account_id:"1",
    profile_id:"ta",requested_datetime:"2015-12-25T12:45:00-04:00",
    pickup:{address_type:"street address",street_no:"10",street_name:"Main St",city:"Warwick",state:"NY",zip_code:"10990",country:"US"},
    dropoff:{address_type:"street address",street_no:"32",street_name:"W 42 St.",city:"Manhattan",state:"NY",zip_code:"10036",country:"US"},
    payment_method:"voucher",special_instructions:"Pick up at side door.",
    booking_criteria:[{booking_criterion_name:"Comp. Req. One",booking_criterion_value:"AAA"},
    {booking_criterion_name:"Comp. Req. Two",booking_criterion_value:"BBB"}]
  }, {json: true})  //If you want to send a raw request body or actual JSON, 
                    //use { json: true } as the third argument (object literal of options).
    .addHeader('api-key',api_key_2)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
       .addHeader('Content-Type','application/json')
        .addHeader('host' , 'dev-clientapi.sedanmagic.com')
         .addHeader('Accept','application/json')
        .expectStatus(201)
          .inspectBody()
            .toss() 




