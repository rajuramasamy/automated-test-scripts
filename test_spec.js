var frisby = require('frisby');
var ClientAPI ="https://dev-clientapi.sedanmagic.com/";
var ProviderAPI ="https://dev-providerapi.sedanmagic.com/";
var VTODAPI = "https://apitest.vtod.com/";
var api_key = '06913B08-5228-4CB2-BD77-E6485C529ACE';
var api_key_2 = 'A69A850F-08F3-4984-A50F-3FC374C37877';
var api_key_provider = '966B3C48-ED51-44DF-91C9-3D36E77378A8';
var auth_tom = 'Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh';
var auth_phaz = 'Basic YWxlcGh0ZXN0MDRAYWxlcGhnbG9iYWwuY29tOlRyYW5zZGV2MTU=';
var auth_suppliers = 'Basic c3VwcGxpZXJzLWludGVncmF0aW9uQGthcmhvby5jb206a2FyaG9vMTIz';
var auth_karhoo = 'Basic c3VwcGxpZXJzLWludGVncmF0aW9uQGthcmhvby5jb206a2FyaG9vMTIz';
var auth_UTOG = 'Basic dG9tYW50b2xhQHRheGltYWdpYy5jb206dGE=';
var acceptHeader = 'application/json';
var hostHeader = 'dev-clientapi.sedanmagic.com';
var hostHeaderProvider = 'dev-providerapi.sedanmagic.com'

//The GlobalSetup() allows common headers to be declared globally
frisby.globalSetup({ 
  request: {
    headers: {'Authorization' : 'Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh'
    , 'Accept' : 'application/json'
    }
  }
});


frisby.create('GET User Profile - 200')
  .get(ClientAPI + 'user/profile')
    .addHeader('accept','application/json')
      .addHeader('api-key',api_key)
        .addHeader('host',hostHeader)
          .addHeader('Authorization',auth_tom)   
            .expectStatus(200)
              .inspectBody() //Inspects the body of the response
                .toss() //Executes the request

frisby.create('POST User Verification - 200')
  .post(ClientAPI + 'user/verification', {
    username: "alephtest04@alephglobal.com"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(200)
            .inspectBody()
              .toss()



frisby.create('POST User Verification - 400 - No username/password')
  .post(ClientAPI + 'user/verification', {
    username: ""
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()

frisby.create('POST User Verification - 404 - User not found')
  .post(ClientAPI + 'user/verification', {
    username: "alephtes04@alephglobal.com"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(404)
            .inspectBody()
              .toss()

//Need to change request's JSON to a new userID
frisby.create('POST User Registration - 201')
  .post(ClientAPI + 'user/ztrip/registration',
    {"username":"thomas.antola@transdev.com",
    "password":"tla",
    "new_password":"tla"},
    {json:true})
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('Authorization', auth_tom)
          .expectStatus(200)
            .inspectBody()
              .toss()           

frisby.create('POST User Registration - 400 - No username/password')
  .post(ClientAPI + 'user/ztrip/registration', {
    username: ""
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()

frisby.create('POST User Registration - 400 - No Username')
  .post(ClientAPI + 'user/ztrip/registration', {
    password: "test"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()

frisby.create('POST User Registration - 400 - No Password')
  .post(ClientAPI + 'user/ztrip/registration', {
    username: "alephtes04@alephglobal.com", password: ""
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()

frisby.create('POST User Registration - 404 - User not found')
  .post(ClientAPI + 'user/ztrip/registration', {
    username: "alephtes04@alephglobal.com", password: "test"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(404)
            .inspectBody()
              .toss()

frisby.create('POST User Registration - 400 - Username too long')
  .post(ClientAPI + 'user/ztrip/registration', {
    username: "alephtes04@alephglobal.comalephtes04@alephglobal.comalephtes04@alephglobal.comalephtes04@alephglobal.com", password: "test"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()

frisby.create('POST User Registration - 400 - Password too long')
  .post(ClientAPI + 'user/ztrip/registration', {
    username: "alephtest04@alephglobal.com", password: "testalephglobalalephglobalalephglobalalephglobalalephglobalalephglobalaleph"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()

frisby.create('POST User Registration - 409 - Conflict')
  .post(ClientAPI + 'user/ztrip/registration', {
    username: "alephtest04@alephglobal.com", password: "test"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(409)
            .inspectBody()
              .toss()            

frisby.create('PUT User Registration - 200 - Password Changed')
  .put(ClientAPI + 'user/ztrip/registration', {
    username: "alephtest04@alephglobal.com", password:"Transdev15", new_password : "test"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(409)
            .inspectBody()
              .toss() 

frisby.create('PUT User Registration - 400 - No Username')
  .put(ClientAPI + 'user/ztrip/registration', {
    username: "", password: "test", new_password: "test"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss() 

frisby.create('PUT User Registration - 400 - No Password')
  .put(ClientAPI + 'user/ztrip/registration', {
    username: "alephtest04@alephglobal.com", new_password: "test"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()

frisby.create('PUT User Registration - 400 - No New Password')
  .put(ClientAPI + 'user/ztrip/registration', {
    username: "alephtest04@alephglobal.com", password:"test"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()

frisby.create('PUT User Registration - 400 - No New Password')
  .put(ClientAPI + 'user/ztrip/registration', {
    username: "alephtest04@alephglobal.com", password:"test"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()

frisby.create('PUT User Registration - 400 - Username too long')
  .put(ClientAPI + 'user/ztrip/registration', {
    username: "alephtest04@alephglobal.comalephtest04@alephglobal.comalephtest04@alephglobal.comalephtest04@alephglobal.com", password:"test", new_password : "test"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()

frisby.create('PUT User Registration - 400 - Password too long')
  .put(ClientAPI + 'user/ztrip/registration', {
    username: "alephtest04@alephglobal.com", password:"test", new_password : "testttttttttttttttttttttttttttttttttttttttttttttttttttttt"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()            

frisby.create('PUT User Registration - 409 - Invalid current password')
  .put(ClientAPI + 'user/ztrip/registration', {
    username: "alephtest04@alephglobal.com", password: "test", new_password: "test"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(409)
            .inspectBody()
              .toss() 

frisby.create('DELETE the user - 400 - No Username')
  .delete(ClientAPI + 'user/ztrip/registration', {
    username: ""
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(400)
            .inspectBody()
              .toss()

frisby.create('DELETE the user - 404 - Username too long/Username not found')
  .delete(ClientAPI + 'user/ztrip/registration', {
    username: "alephtest04@alephglobal.comalephtest04@alephglobal.comalephtest04@alephglobal.comalephtest04@alephglobal.comalephtest04@alephglobal.com"
  })
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(404)
            .inspectBody()
              .toss()

//Provider API --->>>

frisby.create('GET Driver App - 200')
  .get(ProviderAPI + 'driverappconfig/1')
    .addHeader('api-key',api_key_provider)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeaderProvider)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Airport Pickup - 200')
  .get(ProviderAPI + 'airport/pickuppoint')
    .addHeader('api-key',api_key_provider)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeaderProvider)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Final Price - 200')
  .post(ProviderAPI + 'price/final/2/nyone/karhoo/2398372812', 
    {provider_id:2,
      company_name:"NYOne",
      provider_account_id:"Karhoo",
      vendor_confirmation_no:"2398372812",
      final_price:48.75,
      price_components:
        [{component_name:"base rate",
        amount:40.75},
        {component_name:"tips",
        amount:8}]
    }, {json:true})
    .addHeader('api-key',api_key_provider)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeaderProvider)
          .addHeader('Authorization', auth_tom)
            .expectStatus(201)
              .inspectBody()
                .toss()  

//<<<--- Provider API 
 
//Client API - Fleet --->>>

frisby.create('GET Account User Booking Criterion - 200 (Sedan_Magic_Aleph)')
  .get(ClientAPI + 'accountuserbookingcriterion/1/Sedan_Magic_Aleph/1/ta')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(200)
            .inspectBody()
              .toss()

frisby.create('GET Account User Booking Criterion - 200 (Test_Fleet_1)')
  .get(ClientAPI + 'accountuserbookingcriterion/1011/Test_Fleet_1/TD-01/ta')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .expectStatus(200)
            .inspectBody()
              .toss()

frisby.create('GET Account User Status - 200 (Sedan_Magic_Aleph)')
  .get(ClientAPI + 'accountuserstatus/1/Sedan_Magic_Aleph/1/ta')
    .addHeader('api-key',api_key_2)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Content-Type','application/json')
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Account User Status - 200 (Test_Fleet_1)')
  .get(ClientAPI + 'accountuserstatus/1011/Test_Fleet_1/TD-01/ta')
    .addHeader('api-key',api_key_2)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Content-Type','application/json')
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Airport Pickup Points - 200')
  .get(ClientAPI + 'airport/pickuppoint')
    .addHeader('api-key',api_key_2)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Content-Type','application/json')
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Corporation Location Configuration - 200')
  .get(ClientAPI + 'corporate/location/config/40.7484/-73.9857')
    .addHeader('api-key',api_key_2)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Content-Type','application/json')
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Cors - 200')
  .get(ClientAPI + 'cors')
    .addHeader('api-key',api_key_2)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Content-Type','application/json')
            .expectStatus(200)
              .inspectBody()
                .toss()

//Uncomment this with a URI that works
/*frisby.create('GET GPS - 200')
  .get(ClientAPI + 'gps/1/Sedan_Magic_Aleph/1/ta/0102200903')
    .addHeader('api-key',api_key_2)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Content-Type','application/json')
            .expectStatus(200)
              .inspectBody()
                .toss()*/

frisby.create('GET GPS - 404')
  .get(ClientAPI + 'gps/1/Sedan_Magic_Aleph/1/ta/0102200903')
    .addHeader('api-key',api_key_2)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Content-Type','application/json')
            .expectStatus(404)
              .inspectBody()
                .toss()

frisby.create('POST User Registration - 201 - Created (Sedan Magic Aleph)')
  .post(ClientAPI + 'registration/1/Sedan_Magic_Aleph/1/ta/WR-10678B6', {provider_id: "1",
    company_name: "Sedan_Magic_Aleph",
    provider_account_id: "1",
    profile_id: "ta",
    username: "thomas.antola@transdev.com",
    password: "tla",
    email_address: "thomas.antola@transdev.com",
    first_name: "Tom",
    last_name: "Antola",
    device_token: "WR-10678B6",
    phone_number: "973-761-3421",
    platform_name: "Android",
    platform_version: "4.4",
    provider_subaccount_id: "0"
  },{json: true})
    .addHeader('api-key',api_key)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
        .addHeader('Content-Type','application/json')
          .addHeader('host' , hostHeader)
            .addHeader('Accept','application/json')
              .expectStatus(201)
                .inspectBody()
                  .toss()

frisby.create('POST User Registration - 409 - Conflict (Sedan_Magic_Aleph)')
  .post(ClientAPI + 'registration/1/Sedan_Magic_Aleph/1/ta/WR-10678B6', {provider_id: "1011", //Conflicting provider id of 1011 instead of 1
    company_name: "Sedan_Magic_Aleph",
    provider_account_id: "1",
    profile_id: "ta",
    username: "thomas.antola@transdev.com",
    password: "tla",
    email_address: "thomas.antola@transdev.com",
    first_name: "Tom",
    last_name: "Antola",
    device_token: "WR-10678B6",
    phone_number: "973-761-3421",
    platform_name: "Android",
    platform_version: "4.4",
    provider_subaccount_id: "0"
  }, 
  {json: true})
    .addHeader('api-key',api_key)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
        .addHeader('Content-Type','application/json')
          .addHeader('host' , hostHeader)
            .addHeader('Accept',acceptHeader)
              .expectStatus(409)
                .inspectBody()
                  .toss()

frisby.create('POST User Registration - 404 - Not Found (Sedan_Magic_Aleph)')
  .post(ClientAPI + 'registration/1/Test_Fleet_1/1/ta/WR-10678B6', {provider_id: "1011", //Conflicting URI
    company_name: "Sedan_Magic_Aleph",
    provider_account_id: "1",
    profile_id: "ta",
    username: "thomas.antola@transdev.com",
    password: "tla",
    email_address: "thomas.antola@transdev.com",
    first_name: "Tom",
    last_name: "Antola",
    device_token: "WR-10678B6",
    phone_number: "973-761-3421",
    platform_name: "Android",
    platform_version: "4.4",
    provider_subaccount_id: "0"
  }, 
  {json: true})
    .addHeader('api-key',api_key)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
        .addHeader('Content-Type','application/json')
          .addHeader('Accept', acceptHeader)
            .addHeader('host',hostHeader)
              .expectStatus(404)
                .inspectBody()
                  .toss()

frisby.create('POST User Registration - 201 - Created (Test_Fleet_1)')
  .post(ClientAPI + 'registration/1011/Test_Fleet_1/TD-01/ta/WR-10678B7', {provider_id:1011,
    company_name:"Test_Fleet_1",
    provider_account_id:"TD-01",
    profile_id:"ta",
    username:"thomas.antola@transdev.com",
    password:"tla",
    email_address:"thomas.antola@transdev.com",
    first_name:"Tom",
    last_name:"Antola",
    device_token:"WR-10678B7",
    phone_number:"973-761-3421",
    platform_name:"Android",
    platform_version:"4.4",
    provider_subaccount_id:"0"}
    ,{json: true})
    .addHeader('api-key',api_key)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
        .addHeader('Content-Type','application/json')
          .addHeader('Accept', acceptHeader)
            .addHeader('host',hostHeader)
              .expectStatus(201)
                .inspectBody()
                  .toss() 

frisby.create('POST User Registration - 409 - Conflict (Test_Fleet_1)')
  .post(ClientAPI + 'registration/1011/Test_Fleet_1/TD-01/ta/WR-10678B7', {provider_id:1, //Conflicting provider id of 1 instead of 1011
    company_name:"Test_Fleet_1",
    provider_account_id:"TD-01",
    profile_id:"ta",
    username:"thomas.antola@transdev.com",
    password:"tla",
    email_address:"thomas.antola@transdev.com",
    first_name:"Tom",
    last_name:"Antola",
    device_token:"WR-10678B7",
    phone_number:"973-761-3421",
    platform_name:"Android",
    platform_version:"4.4",
    provider_subaccount_id:"0"}
    ,{json: true})
    .addHeader('api-key',api_key)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
        .addHeader('Content-Type','application/json')
          .addHeader('Accept', acceptHeader)
            .addHeader('host',hostHeader)
              .expectStatus(409)
                .inspectBody()
                  .toss()            

frisby.create('POST User Registration - 404 - Not Found (Test_Fleet_1)')
  .post(ClientAPI + 'registration/1011/Sedan_Magic_Aleph/TD-01/ta/WR-10678B7', {provider_id:1011, //Conflicting URI
    company_name:"Test_Fleet_1",
    provider_account_id:"TD-01",
    profile_id:"ta",
    username:"thomas.antola@transdev.com",
    password:"tla",
    email_address:"thomas.antola@transdev.com",
    first_name:"Tom",
    last_name:"Antola",
    device_token:"WR-10678B7",
    phone_number:"973-761-3421",
    platform_name:"Android",
    platform_version:"4.4",
    provider_subaccount_id:"0"}
    ,{json: true})
    .addHeader('api-key',api_key)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
        .addHeader('Content-Type','application/json')
          .addHeader('Accept', acceptHeader)
            .addHeader('host',hostHeader)
              .expectStatus(404)
                .inspectBody()
                  .toss() 

frisby.create('POST User Registration - 201 - Created (Karhoo)')
  .post(ClientAPI + 'registration/1011/Test_Fleet_1/Karhoo/SA-Karhoo/TestDevice', {provider_id:1011,
    company_name:"Test_Fleet_1",
    provider_account_id:"Karhoo",
    profile_id:"SA-Karhoo",
    username:"suppliers-integration@karhoo.com",
    password:"karhoo123",
    email_address:"suppliers-integration@karhoo.com",
    first_name:"Infrastructure",
    last_name:"Karhoo",
    device_token:"TestDevice",
    phone_number:"777-999-0000",
    platform_name:"Android",
    platform_version:"4.4",
    provider_subaccount_id:"0"}
    ,{json: true})
    .addHeader('api-key',api_key)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
        .addHeader('Content-Type','application/json')
          .addHeader('Accept', acceptHeader)
            .addHeader('host',hostHeader)
              .expectStatus(201)
                .inspectBody()
                  .toss() 

frisby.create('POST User Registration - 409 - Conflict (Karhoo)')
  .post(ClientAPI + 'registration/1011/Test_Fleet_1/Karhoo/SA-Karhoo/TestDevice', {provider_id:1, //Conflicting provider id of 1 instead of 1011
    company_name:"Test_Fleet_1",
    provider_account_id:"Karhoo",
    profile_id:"SA-Karhoo",
    username:"suppliers-integration@karhoo.com",
    password:"karhoo123",
    email_address:"suppliers-integration@karhoo.com",
    first_name:"Infrastructure",
    last_name:"Karhoo",
    device_token:"TestDevice",
    phone_number:"777-999-0000",
    platform_name:"Android",
    platform_version:"4.4",
    provider_subaccount_id:"0"}
    ,{json: true})
    .addHeader('api-key',api_key)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
        .addHeader('Content-Type','application/json')
          .addHeader('Accept', acceptHeader)
            .addHeader('host',hostHeader)
              .expectStatus(409)
                .inspectBody()
                  .toss() 

frisby.create('POST User Registration - 404 - Not Found (Karhoo)')
  .post(ClientAPI + 'registration/1011/Sedan_Magic_Aleph/Karhoo/SA-Karhoo/TestDevice', {provider_id:1, //Conflicting URI
    company_name:"Test_Fleet_1",
    provider_account_id:"Karhoo",
    profile_id:"SA-Karhoo",
    username:"suppliers-integration@karhoo.com",
    password:"karhoo123",
    email_address:"suppliers-integration@karhoo.com",
    first_name:"Infrastructure",
    last_name:"Karhoo",
    device_token:"TestDevice",
    phone_number:"777-999-0000",
    platform_name:"Android",
    platform_version:"4.4",
    provider_subaccount_id:"0"}
    ,{json: true})
    .addHeader('api-key',api_key)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
        .addHeader('Content-Type','application/json')
          .addHeader('Accept', acceptHeader)
            .addHeader('host',hostHeader)
              .expectStatus(404)
                .inspectBody()
                  .toss()

//TBC - Returns a 200 even if username does not exist. No payload with response.
frisby.create('POST Registration Verification')
  .post(ClientAPI + 'registrationverification/1/Sedan_Magic_Aleph/1/ta', {username:"thomas.antola@transdev.com"})
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('POST User Reservation - 201 - Created (Sedan_Magic_Aleph)')
  .post(ClientAPI + 'reservation/1/Sedan_Magic_Aleph/1/ta', 
    {provider_id:"1",
    company_name:"Sedan_Magic_Aleph",
    provider_account_id:"1",
    profile_id:"ta",
    requested_datetime:"2015-12-25T12:45:00-04:00",
    pickup:{address_type:"street address",
    street_no:"10",
    street_name:"Main St",
    city:"Warwick",
    state:"NY",
    zip_code:"10990",
    country:"US"},
    dropoff:{address_type:"street address",
    street_no:"32",
    street_name:"W 42 St.",
    city:"Manhattan",
    state:"NY",
    zip_code:"10036",
    country:"US"},
    payment_method:"voucher",
    special_instructions:"Pick up at side door.",
    booking_criteria:[{booking_criterion_name:"Comp. Req. One",
    booking_criterion_value:"AAA"},
    {booking_criterion_name:"Comp. Req. Two",
    booking_criterion_value:"BBB"}]
  }, {json: true})  //If you want to send a raw request body or actual JSON, 
                    //use { json: true } as the third argument (object literal of options).
    .addHeader('api-key',api_key_2)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
        .addHeader('Content-Type','application/json')
          .addHeader('Accept', acceptHeader)
            .addHeader('host',hostHeader)
              .expectStatus(201)
                .inspectBody()
                  .toss()            

//Need to investigate booking criteria and then change expectStatus to 201 from 400
frisby.create('POST User Reservation - 201 - Created (Test_Fleet_1)')
  .post(ClientAPI + 'reservation/1011/Test_Fleet_1/TD-01/ta', 
    {provider_id:1011,
    company_name:"Test_Fleet_1",
    provider_account_id:"TD-01",
    profile_id:"ta",
    requested_datetime:"2015-12-25T12:45:00-04:00",
    pickup:
      {address_type:"street address",
      street_no:"10",
      street_name:"Main St",
      city:"Warwick",
      state:"NY",
      zip_code:"10990",
      country:"US"},
    dropoff:
      {"address_type":"street address",
      street_no:"32",
      street_name:"W 42 St.",
      city:"Manhattan",
      state:"NY",
      zip_code:"10036",
      country:"US"},
    payment_method:"voucher",
    special_instructions:"Pick up at side door.",
    booking_criteria:
      [{booking_criterion_name:"BILLING  CODE",
      booking_criterion_value:"A"},
      {booking_criterion_name:"BUDGET CODE",
      booking_criterion_value:"B"},
      {booking_criterion_name:"CHARGE CODE",
      booking_criterion_value:"C"},
      {booking_criterion_name:"CLIENT CODE 1",
      booking_criterion_value:"D"},
      {booking_criterion_name:"CLIENT CODE 2",
      booking_criterion_value:"E"},
      {booking_criterion_name:"CLIENT CODE 3",
      booking_criterion_value:"F"}]
    }, {json: true})  //If you want to send a raw request body or actual JSON, 
                    //use { json: true } as the third argument (object literal of options).
    .addHeader('api-key',api_key_2)
      .addHeader('Authorization','Basic dGhvbWFzLmFudG9sYUB0cmFuc2Rldi5jb206dGxh')
        .addHeader('Content-Type','application/json')
          .addHeader('Accept', acceptHeader)
            .addHeader('host',hostHeader)
              .expectStatus(400)
                .inspectBody()
                  .toss()            

frisby.create('GET Available Vehicles - 200')
  .get(ClientAPI + 'vehicle/available/40.736038/-73.993571/.5/10')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('POST Price Request - 201 - Created')
  .post(ClientAPI + 'price/request/karhoo/karhoo', 
    {corporation_name:"karhoo",
    account_name:"karhoo",
    pickup_location_type:"airport",
    pickup_location:{IATA_airport_code:"EWR",airport_pickup_type:"standby"},
    stops:[{stop_location_type:"street_address",stop_location:{street_no:"12",street_name:"Wall Street",city:"New York",state:"NY",zip_code:"10005",country:"USA",latitude:40.707451,longitude:-74.011187}}],
    dropoff_location_type:"as_directed",dropoff_location:{time_requested_in_minutes:75},
    requested_datetime:"2014-12-11T13:45:00-04:00",
    primary_passenger:{first_name:"Tom",last_name:"Antola",profile_id:"ta",email_address:"thomas.antola@transdev.com"},
    passenger_count:1,luggage_count:0,requested_vehicle_types:["sedan"]}, {json: true})  //If you want to send a raw request body or actual JSON, 
                    //use { json: true } as the third argument (object literal of options).
    .addHeader('api-key',api_key_2)
      .addHeader('Authorization',auth_karhoo)
       .addHeader('Content-Type','application/json')
        .addHeader('host' , 'dev-clientapi.sedanmagic.com')
         .addHeader('Accept','application/json')
          .expectStatus(201)
           .inspectBody()
            .toss()

//<<<--- Client API - Fleet

          

//Client API - Corporate --->>>>

frisby.create('GET Corporate Booking Criteria - 200')
  .get(ClientAPI + 'corporate/bookingcriteria')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Corporate Booking Criteria - 200 - One Account')
  .get(ClientAPI + 'corporate/bookingcriteria/One%20Account%2C%20Inc')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Corporate Location Configuration - 200')
  .get(ClientAPI + 'corporate/location/config/40.736038/-73.993571')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Corporation Requirements - 200')
  .get(ClientAPI + 'corporate/requirements')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Corporation Requirements - 200 - One Account Inc')
  .get(ClientAPI + 'corporate/requirements/One%20Account,%20Inc')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Corporation Requirements - 200 - One Account Inc Test Account')
  .get(ClientAPI + 'corporate/requirements/One%20Account,%20Inc/Test%20Account')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Corporation Requirements - 200 - One Account Inc Test Account BILLING CODE')
  .get(ClientAPI + 'corporate/requirements/One%20Account,%20Inc/Test%20Account/BILLING%20%20CODE')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

//Does not return any payload with the response                                                                    
frisby.create('GET Corporation Account Requirements Typeahead - 200')
  .get(ClientAPI + 'corporate/requirements/typeahead/Plexitech%20Technologies%20Pvt.%20Ltd/Plexitech%20Test%20Account/DIAGNOSIS%20CODE/2/b')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET Corporation Account Payment Method - 200')
  .get(ClientAPI + 'corporate/paymentmethod')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

//Does not return any payload with the response   
frisby.create('GET Corporation Account Payment Method - 200 - TransdevNA')
  .get(ClientAPI + 'corporate/paymentmethod/TransdevNA')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()            

frisby.create('GET Corporation Account Payment Method - 200 - TransdevNA Super Shuttle')
  .get(ClientAPI + 'corporate/paymentmethod/TransdevNA/Super%20Shuttle')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

//Need to add /{username}/{corporation_name} to the URI
frisby.create('GET Corporate Booking Option - 200')
  .get(ClientAPI + 'corporate/bookingoption/40.736038/-73.993571/')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()            

frisby.create('GET Corporation Reservations - Get Reservation List - 200')
  .get(ClientAPI + 'corporate/reservation')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET User Profiles - 200')
  .get(ClientAPI + 'user/profile')
    .addHeader('api-key',api_key)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

//<<<--- Client API - Corporate

//Testing Utog with Ali --->>>

frisby.create('POST Testing Utog with Ali - 401')
  .post(ClientAPI + 'reservation/115/Utog/10000/ta/1500088627', 
    {provider_id:115,
    company_name:"Utog",
    provider_account_id:"10000",
    profile_id:"ta",
    vendor_confirmation_no:"1500088627",
    agent_ride_no:"GR-10236",
    originating_system_reference_number:"GR-10236",
    requested_datetime:"2015-10-10T09:30:00-05:00",
    cancelled_timestamp:null,
    pickup:
    {street_no:"3",
    street_name:"Brewster Rd",
    city:"Newark",
    state:"NJ",
    zip_code:"07114",
    country:"USA",
    auto_complete:"10 Main Street Warwick, NY 10990",
    address_type:"Airport",
    airport_code:"EWR",
    airline:"American",
    flight_number:"2046",
    pickup_point:null,
    airport_pickup_point:"Standby",
    city_of_origin:"Newark",
    latitude:41.306085,
    longitude:-74.44756},
    dropoff:
    {street_no:"32",
    street_name:"W 42 St",
    city:"Manhattan",
    state:"NY",
    zip_code:"10036",
    country:"USA",
    auto_complete:"W 42 St New York, NY 10036",
    address_type:"Street Address",
    airport_code:null,
    latitude:41.38673,
    longitude:-74.452986},
    payment_method_id:1,
    payment_method:"Voucher",
    special_instructions:"THIS IS A TEST.  DO NOT DISPATCH...",
    provider_vehicle_id:null,
    is_asap:false,
    is_dropoff_as_directed:false,
    provider_subaccount_id:"0"}, 
 {json: true})
    .addHeader('api-key',api_key_2)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_UTOG)
            .expectStatus(401)
              .inspectBody()
                .toss()

//<<<--- Testing Utog with Ali

//VTOD API - Notification --->>>

frisby.create('GET VTOD API - REST/GetToken - 200')
  .post(VTODAPI + '2.0.0/REST/GetToken', 
    {Version: "2.0.0",
    Username: "Aleph",
    Password: "AL3ph ]gLobAL"}, {json: true})
    .addHeader('api-key',api_key_2)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .expectStatus(200)
              .inspectBody()
                .toss()

frisby.create('GET VTOD API - Notification - 200')
  .post(VTODAPI + 'Utility/1.0.0/Notification/', 
    {
    Text: "Test Message 3",
    PushType: "Push",
    EmailAddress: "thomas.antola@transdev.com"},
    {json: true})
    .addHeader('api-key',api_key_2)
      .addHeader('Accept', acceptHeader)
        .addHeader('host',hostHeader)
          .addHeader('Authorization', auth_tom)
            .addHeader('SecurityKey', 'd75146b3-2948-4fd4-89aa-d86c45c9127d')
              .addHeader('Username', 'Aleph')
                .expectStatus(400)
                  .inspectBody()
                    .toss()

//<<<--- VTOD API - Notification