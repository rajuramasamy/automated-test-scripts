# README #

Automated Test Scripts using NodeJS.

### What is this repository for? ###

* Summary - This repository contains scripts and proof of concepts designed to automate API testing using NodeJS as the primary framework and including various modules including but not limited to Jasmine, Frisby (built on top of Jasmine), Express.
* Version - 1.0

### How do I get set up? ###

* Summary of set up - Install npm, jasmine and frisby. 
                                                   1. NPM - https://www.npmjs.com/package/npm (if using Windows, install the MSI package)
                                                   2. Jasmine and Frisby - http://frisbyjs.com/ (after installing NPM, use a CLI to run **"npm install --save-dev frisby"** and **"npm install -g jasmine-node"**

* Editor - Install Submlime Text [here](http://www.sublimetext.com/2).

* CLI - Install Cygwin Terminal (for Windows only) [here](https://cygwin.com/install.html).

* How to run tests - Open a CLI and run **jasmine-node <scriptname_spec.js>** (Note that all scripts should follow the naming convention of '_spec.js' after their name so that Jasmine can identify them).

* How to generate reports - The jasmine-node test runner has an option that generates test run reports in JUnit XML format. This format is compaitlble with most CI servers, including Hudson, Jenkins, Bamboo, Pulse, and others - '**jasmine-node <scriptname_spec.js> --junitreport**' 

* Jasmine now supports running multiple scripts at the same time: **jasmine-node <script1_spec.js> <script2_spec.js> <script3_spec.js>**

* The way it is done: **jasmine-node test_spec.js NoAuthorizationHeader_spec.js NoAcceptHeader_spec.js --junitreport**

### Who do I talk to? ###

* Prashanth Ashok Ramkumar - Prashanth.AshokRamKumar@transdev.com
* Tom Antola - Thomas.Antola@transdev.com